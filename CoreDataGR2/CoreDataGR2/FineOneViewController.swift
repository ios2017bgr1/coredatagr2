//
//  FineOneViewController.swift
//  CoreDataGR2
//
//  Created by Sebas on 2/2/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import CoreData

class FineOneViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!

    private let managedObjectContext =
        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var persona:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = persona?.name
        
        addressLabel.text = persona?.address!
        phoneLabel.text = persona?.phone!
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        
        managedObjectContext.delete(persona!)
        
        do {
            try managedObjectContext.save()
            navigationController?.popViewController(animated: true)
        } catch {
            print("Error al eliminar")
        }
    }
}













