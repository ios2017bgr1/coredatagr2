//
//  ViewController.swift
//  CoreDataGR2
//
//  Created by Sebas on 29/1/18.
//  Copyright © 2018 Sebas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var persona:Person?
    
    
    private let managedObjectContext =
        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: managedObjectContext)
        
        person.name = nameTextField.text!
        person.address = addressTextField.text!
        person.phone = phoneTextField.text!
        
        do {
            
            try managedObjectContext.save()
            clearFields()
            
        } catch let error{
            print(error)
            clearFields()
        }
        
    }
    
    func clearFields() {
        nameTextField.text = ""
        addressTextField.text = ""
        phoneTextField.text = ""
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        
        if nameTextField.text == "" {
            fetchAll()
            return
        }
        
        fetchOne()
   
    }
    
    func fetchOne() {
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = pred
        
        do {
            let results = try managedObjectContext.fetch(request)
            if results.count > 0 {
                persona = results[0]
//                addressTextField.text = person.address!
//                phoneTextField.text = person.phone!
                performSegue(withIdentifier: "findOneSegue", sender: self)
            }
            
        } catch let error{
            print(error)
        }
        
    }
    
    func fetchAll() {
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let results = try managedObjectContext.fetch(request)
            
            for p in results {
                
                let person = p as! Person
                
                print("Nombre: \(person.name!) -- ", terminator:"")
                print("Teléfono: \(person.phone!) -- ", terminator:"")
                print("Dirección: \(person.address!) ", terminator:"")
                
                print()
                print()
            }
            
        } catch let error {
            print(error)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "findOneSegue"){
            let destination = segue.destination as! FineOneViewController
            destination.persona = persona!
        }
    }

}

















